/*
 * MIT License
 *
 * Copyright (c) 2021 HorribleNerd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.horriblenerd.wwta;

import net.minecraft.network.chat.TextColor;
import net.minecraft.network.chat.TextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Calendar;
import java.util.UUID;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(WWTA.MODID)
public class WWTA {
    // Directly reference a log4j logger.
    private static final Logger LOGGER = LogManager.getLogger();
    public static final String MODID = "wwta";

    public WWTA() {
        ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, Config.CLIENT_CONFIG);
        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
    }

    @OnlyIn(Dist.CLIENT)
    @SubscribeEvent
    public void onChat(ClientChatReceivedEvent event) {
        Calendar cal = Calendar.getInstance();

        String timeString;
        int hours;
        if (Config.TIME_FORMAT.get()) {
            hours = cal.get(Calendar.HOUR_OF_DAY);
        }
        else {
            hours = cal.get(Calendar.HOUR);
        }
        if (Config.SHOW_SECONDS.get()) {
            timeString = String.format("[%02d:%02d:%02d", hours, cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
        }
        else {
            timeString = String.format("[%02d:%02d", hours, cal.get(Calendar.MINUTE));
        }
        if (!Config.TIME_FORMAT.get() && Config.SHOW_AM_PM.get()) {
            timeString += " " + (cal.get(Calendar.AM_PM) == Calendar.PM ? "PM" : "AM");
        }
        timeString += "]";

        TextComponent finalString = new TextComponent(timeString);
        TextColor color;

        if (Config.NAME_BASED.get() && event.getSenderUUID() != null) {
            color = getColorFromUUID(event.getSenderUUID());
        }
        else {
            color = TextColor.fromRgb(Config.COLOR.get());
        }

        finalString.setStyle(finalString.getStyle().withColor(color));

        if (Config.SHOW_BEFORE.get()) {
            event.setMessage(new TextComponent("").append(finalString).append(" ").append(event.getMessage()));
        }
        else {
            event.setMessage(new TextComponent("").append(event.getMessage()).append(" ").append(finalString));
        }
    }

    private TextColor getColorFromUUID(UUID senderUUID) {
        return TextColor.fromRgb(senderUUID.hashCode());
    }

}
